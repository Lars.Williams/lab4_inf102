package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
    	int listSize = list.size();
        boolean swapped;
        do {
            swapped = false;
            for (int i = 1; i < listSize; i = i + 1) {
            	// check the value of current number in lists against previous number
                if (list.get(i - 1).compareTo(list.get(i)) > 0) {
                    // swap positions if previous number is larger
                    T temporarySave = list.get(i - 1);
                    list.set(i - 1, list.get(i));
                    list.set(i, temporarySave);
                    swapped = true;
                }
            }
            // each loop puts the biggest value at the end. can remove it from future loops
            listSize = listSize -1;
        // do {} while (swapped) will make the process end if no terms are swapped
        // this means that unnecessary loops can be prevented if the numbers are done sorting early
        } while (swapped);
    }
    
}
