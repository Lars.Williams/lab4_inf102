package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        int listSize = listCopy.size();
        int halfSize = listSize / 2; // The index of the median element

        int left = 0;
        int right = listSize - 1;
        while (left <= right) {
            int pivotIndex = left + (right - left) / 2;
            T pivotValue = listCopy.get(pivotIndex);

            int storeIndex = left;

            // Swap the pivot value with the value at the rightmost index
            T temp = listCopy.get(pivotIndex);
            listCopy.set(pivotIndex, listCopy.get(right));
            listCopy.set(right, temp);

            for (int i = left; i < right; i = i + 1) {
                if (listCopy.get(i).compareTo(pivotValue) < 0) {
                    T temp2 = listCopy.get(i);
                    listCopy.set(i, listCopy.get(storeIndex));
                    listCopy.set(storeIndex, temp2);
                    storeIndex = storeIndex + 1;
                }
            }

            T temp3 = listCopy.get(storeIndex);
            listCopy.set(storeIndex, listCopy.get(right));
            listCopy.set(right, temp3);

            if (halfSize == storeIndex) {
                return listCopy.get(halfSize);
            } else if (halfSize < storeIndex) {
                right = storeIndex - 1;
            } else {
                left = storeIndex + 1;
            }
        }

        // need to handle the case when the list has an even number of elements
        if (listSize % 2 == 0) {
            T median1 = listCopy.get(halfSize - 1);
            T median2 = listCopy.get(halfSize);
            return (median1.compareTo(median2) < 0) ? median1 : median2;
        } else {
            return listCopy.get(halfSize);
        }
    }
}